document.getElementById('start-game').addEventListener('click', function() {
    const secretWord = document.getElementById('secret-word').value.toUpperCase();
    if (secretWord.length > 0) {
        setupGame(secretWord);
        preFillFirstLetter(secretWord);
    }
});

function setupGame(secretWord) {
    document.getElementById('setup').style.display = 'none';

    const gameBoard = document.getElementById('game-board');
    gameBoard.innerHTML = ''; // On vide la grille avant de la générer
    gameBoard.style.display = 'grid';
    gameBoard.style.gridTemplateColumns = `repeat(${secretWord.length}, 1fr)`;

    for (let row = 0; row < 6; row++) {
        for (let col = 0; col < secretWord.length; col++) {
            const cell = document.createElement('div');
            cell.classList.add('cell');
            cell.setAttribute('data-row', row);
            cell.setAttribute('data-col', col);
            gameBoard.appendChild(cell);
        }
    }

    // Afficher le clavier virtuel
    createKeyboard(secretWord.length);
}

function createKeyboard(wordLength) {
    const keyboard = document.getElementById('keyboard');
    keyboard.style.display = 'block';
    keyboard.innerHTML = ''; // Vide le clavier pour éviter les doublons

    const keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    keys.forEach(key => {
        const keyButton = document.createElement('div');
        keyButton.classList.add('keyboard-key');
        keyButton.textContent = key;
        keyButton.addEventListener('click', () => {
            handleKeyPress(key, wordLength);
        });
        keyboard.appendChild(keyButton);
    });

    // Ajouter la touche "Effacer"
    const deleteButton = document.createElement('div');
    deleteButton.classList.add('keyboard-key', 'delete-key');
    deleteButton.textContent = 'Effacer';
    deleteButton.addEventListener('click', () => {
        handleDeleteKeyPress(wordLength);
    });
    keyboard.appendChild(deleteButton);
}

function preFillFirstLetter(secretWord) {
    const firstCell = document.querySelector('.cell[data-row="0"][data-col="0"]');
    if (firstCell) {
        firstCell.textContent = secretWord[0];
        firstCell.classList.add('filled', 'correct');
        currentGuess += secretWord[0];
        // On marque la lettre comme remplie pour éviter d'ajouter des lettres supplémentaires
        document.querySelector('.cell[data-row="0"][data-col="0"]').classList.add('filled');
        document.getElementById('submit-word').style.display = 'block'; // Affiche le bouton de soumission
    }
}

let currentGuess = '';
let currentRow = 0;

function handleKeyPress(letter, wordLength) {
    const gameBoard = document.getElementById('game-board');
    const emptyCells = Array.from(gameBoard.querySelectorAll(`.cell[data-row="${currentRow}"]:not(.filled)`));

    if (emptyCells.length > 0 && currentGuess.length < wordLength) {
        const nextCell = emptyCells[0];
        nextCell.textContent = letter;
        nextCell.classList.add('filled');
        currentGuess += letter;
    }

    if (currentGuess.length === wordLength) {
        document.getElementById('submit-word').style.display = 'block';
    }
}

// Ajout de la fonction de suppression de la dernière lettre
function handleDeleteKeyPress(wordLength) {
    const filledCells = Array.from(document.querySelectorAll(`.cell[data-row="${currentRow}"].filled`));

    if (filledCells.length > 1 || (filledCells.length === 1 && currentGuess.length !== 1)) { // On ne veut pas supprimer la première lettre préremplie
        const lastFilledCell = filledCells[filledCells.length - 1];
        lastFilledCell.textContent = '';
        lastFilledCell.classList.remove('filled');
        currentGuess = currentGuess.slice(0, -1);

        if (currentGuess.length < wordLength) {
            document.getElementById('submit-word').style.display = 'none';
        }
    }
}

// Validation progressive des lettres et correction des lettres
document.getElementById('submit-word').addEventListener('click', function() {
    const secretWord = document.getElementById('secret-word').value.toUpperCase();
    const rowCells = Array.from(document.querySelectorAll(`.cell[data-row="${currentRow}"]`));
    
    let i = 0;
    let validatedLetters = [];
    let usedSecretWordLetters = Array(secretWord.length).fill(false); // Marqueur pour éviter les doublons de lettres mal placées

    function validateLetter() {
        if (i < rowCells.length) {
            const cell = rowCells[i];
            const guessedLetter = cell.textContent;

            if (guessedLetter === secretWord[i]) {
                cell.classList.add('correct');
                validatedLetters[i] = guessedLetter; // Sauvegarde la lettre correcte
                usedSecretWordLetters[i] = true; // Marque cette lettre dans le mot secret
            }
            i++;
            setTimeout(validateLetter, 300); // Effet machine à écrire
        } else {
            // Deuxième passe : lettres mal placées (jaune)
            for (let j = 0; j < rowCells.length; j++) {
                const cell = rowCells[j];
                const guessedLetter = rowCells[j].textContent;

                if (!cell.classList.contains('correct') && secretWord.includes(guessedLetter)) {
                    const secretIndices = [];
                    for (let k = 0; k < secretWord.length; k++) {
                        if (secretWord[k] === guessedLetter && !usedSecretWordLetters[k]) {
                            secretIndices.push(k);
                        }
                    }

                    if (secretIndices.length > 0) {
                        cell.classList.add('misplaced');
                        usedSecretWordLetters[secretIndices[0]] = true; // Marque cette lettre pour éviter les doublons
                    } else {
                        cell.classList.add('incorrect');
                    }
                } else if (!cell.classList.contains('correct')) {
                    cell.classList.add('incorrect');
                }
            }

            // Vérifier si le mot est correct
            const isCorrect = rowCells.every(cell => cell.classList.contains('correct'));
            if (isCorrect) {
                setTimeout(() => alert('Félicitations ! Vous avez trouvé le mot !'), 500);
                document.getElementById('submit-word').style.display = 'none';
                return; // Arrête le traitement de la ligne suivante
            }

            // Pré-remplissage de la ligne suivante avec les lettres validées (mais pas celles mal placées)
            currentGuess = '';
            currentRow++;
            document.getElementById('submit-word').style.display = 'block'; // Assure que le bouton reste visible

            if (currentRow < 6) {
                const nextRowCells = Array.from(document.querySelectorAll(`.cell[data-row="${currentRow}"]`));
                nextRowCells.forEach((cell, index) => {
                    if (validatedLetters[index]) {
                        cell.textContent = validatedLetters[index]; // Pré-remplit avec la lettre correcte
                        cell.classList.add('correct');
                        cell.classList.add('filled');
                    }
                });
            }
        }
    }
    
    validateLetter();
});
